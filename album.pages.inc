<?php

function album_year_page_list() {

  drupal_add_library('system', 'drupal.ajax');
  drupal_add_css(drupal_get_path('module', 'album_hierarchy') . '/album.css');
  drupal_add_js(drupal_get_path('module', 'album_hierarchy') . '/js/album.js');

  $result = db_select('node', 'n')
    ->fields('n', array('nid','created'))
    ->condition('type', 'album','=')
    ->condition('status', 0,'>')
    ->orderBy('created', 'DESC')
    ->execute();
  
  $data = array();
  $data['created_year'] = array();

  while($node = $result->fetchAssoc()) {
    $year = date('Y',$node['created']);
    if (!in_array($year, $data['created_year']))
      $data['created_year'][] = $year;
  }

  return theme('album_year_page_list', $data);
}

function album_month_page_list($ajax, $year) {

  $is_ajax = $ajax === 'ajax';

  $result = db_select('node', 'n')
    ->fields('n', array('nid','created'))
    ->condition('type', 'album','=')
    ->condition('status', 0,'>')
    ->where('YEAR(FROM_UNIXTIME(n.created)) = :created', array(':created' => $year))
    ->orderBy('created', 'DESC')
    ->execute();

  $data = array();
  $data['created_month'] = array();

  while($node = $result->fetchAssoc()) {
    $month_year = date('M Y',$node['created']);
    if (!in_array($month_year, $data['created_month']))
      $data['created_month'][$node['created']] = $month_year;
  }
  
  if ($is_ajax) {
    $commands = array();

    //drupal_add_library('system', 'drupal.ajax');

    $month_result = theme('album_month_page_list', $data);

    $commands[] = ajax_command_replace('.album-list', $month_result);
    $commands[] = ajax_command_html('h1#page-title', t('Album').' - '.$year);
    
    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
  }
  else {
    return theme('album_month_page_list', $data);
  }
}

function album_image_page_list($ajax, $year, $month) {

  $is_ajax = $ajax === 'ajax';

  $query = db_select('node', 'n');
  $query->leftjoin('field_data_field_album_image', 'fdfai', 'n.nid=fdfai.entity_id');
  $query->leftjoin('file_managed', 'fm', 'fdfai.field_album_image_fid=fm.fid');
  $query->fields('n', array('nid','title'));
  $query->fields('fm');
  $query->condition('n.type', 'album','=');
  $query->condition('n.status', 1,'=');
  $query->condition('fdfai.deleted', 0,'=');
  $query->condition('fdfai.language', 'und','=');
  $query->condition('fm.status', 1,'=');
  $query->where('YEAR(FROM_UNIXTIME(n.created)) = :created_year AND MONTH(FROM_UNIXTIME(n.created)) = :created_month', array(':created_year' => $year,':created_month' => $month));
  $query->orderBy('n.created', 'DESC');
  $result = $query->execute();

  $data = array();
  $data['nodes'] = array();

  while($node = $result->fetchAssoc()) {
    $data['nodes'][] = $node;
  }

  if ($is_ajax) {
    $commands = array();

    //drupal_add_library('system', 'drupal.ajax');

    $image_result = theme('album_image_page_list', $data);

    $commands[] = ajax_command_replace('.album-list', $image_result);
    $commands[] = ajax_command_html('h1#page-title', t('Album').' - '.$year);
    
    return array(
      '#type' => 'ajax',
      '#commands' => $commands,
    );
  }
  else {
    return theme('album_image_page_list', $data);
  }
}