<?php

/**
 * @file
 * Install, update and uninstall functions for the album_hierarchy module.
 */

/**
 * Implements hook_install().
 */
function album_hierarchy_install() {
	//  The machine name of the content type can contain only lowercase alphanumeric
	//  characters and underscores.
	$type_name = 'album';

	//  Verify the content type does not already exist. To instead get the node types
	//  as objects, use node_type_get_types().
	if ( in_array( $type_name, node_type_get_names() ) ) {
	    return;
	}

	//  Create the type definition array.
	$type = array(
	    'type' => $type_name,
	    'name' => t('Album'),
	    'base' => 'node_content',
	    'description' => t('For creating albums and upload images into it.'),
	    'custom' => 1,
	    'modified' => 1,
	    'locked' => 0,
	);
	$type = node_type_set_defaults( $type );

	// Check if our field is not already created.
  	if (!field_info_field('field_album_image')) {

		// Create an image field
	    $field = array(
	      'field_name' => 'field_album_image',
	      'type' => 'image',
	      'entity_types' => array('node'),
	    );
	    $field = field_create_field($field);
	    $instance = array(
	      'field_name' => 'field_album_image',
	      'entity_type' => 'node',
	      'bundle' => $type_name,
	      'label' => t('Album Image'),
	      'description' => 'An image for the album.',
	      'required' => TRUE,
	      'settings' => array(
		    'file_extensions' => 'png jpg jpeg',
		    'file_directory' => '',
		    'max_filesize' => '10 MB',
		    'alt_field' => 1,
		    'title_field' => 1,
		    'default_image' => 0,
		  ),
	    );
	    $instance = field_create_instance($instance);
    }

	node_type_save( $type );
	// Add a body field.
	node_add_body_field( $type );
}

/**
 * Implements hook_uninstall().
 */
function album_hierarchy_uninstall() {

	$result = db_select('node', 'n')
		->fields('n', array('nid'))
		->condition('type', 'album','=')
		->execute();

	$nids = array();
	while($row = $result->fetchAssoc()) {
		$nids[] = $row['nid'];
	}
	// Delete all the nodes at once
	node_delete_multiple($nids);
	// Delete our content type
	node_type_delete('album');
}
