<div class="album-list">

	<ul id="albums" class="clearfix">
	  <?php foreach($created_year as $key => $year) { 
	  	$img_path = drupal_get_path('module','album_hierarchy');
	  	?>
	  <li class="<?php print ($key==0) ? 'first ' : ''; ?>album album-year album-<?php print $year; ?>">
	    <a href="album/nojs/<?php print $year; ?>" class="use-ajax">
	      <img src="<?php print base_path().drupal_get_path('module', 'album_hierarchy'); ?>/images/folder.png"/>
	      <?php print $year; ?>
	    </a>
	  </li>
	  <?php } ?>
	</ul>
</div>