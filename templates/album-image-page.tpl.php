<div class="album-list">
	<a href="album"><?php print t('>> Back'); ?></a>
	<ul id="albums" class="clearfix">
	  <?php 
	  foreach($nodes as $key => $node) { ?>
	   <li class="<?php print ($key==0) ? 'first ' : ''; ?>album album-month album-<?php print isset($month) ? $month : ''; ?>">
	    <a class="image_colorbox" href="#inline_content<?php print $key; ?>" title="<?php print $node['title']; ?>">
	    <?php 
		    print theme('image_style', array('style_name' => 'thumbnail', 'path' => $node['uri']));
		    print $node['title'];
	    ?>
	    </a>

	    <!-- This contains the hidden content for inline calls -->
		<div style='display:none'>
			<div id='inline_content<?php print $key; ?>'>
				<div class="inline-image">
					<?php 
				    print theme('image_style', array('style_name' => 'large', 'path' => $node['uri']));
				    ?>
				</div>
				<div class="inline-comment">
					<?php
					$comp_node = node_load($node['nid']);
					$form_comment = drupal_get_form("comment_node_{$comp_node->type}_form", (object) array('nid' => $comp_node->nid));
					//print drupal_render(comment_node_load($comp_node));
					//print "<pre>"; print_r($comp_node); die;
					//print comment_render($comp_node);
					print drupal_render($form_comment);
					?>
				</div>
			</div>
		</div>

	  </li>
	  <?php } ?>
	</ul>
</div>