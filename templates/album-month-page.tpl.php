<div class="album-list">
	<a href="album"><?php print t('>> Back'); ?></a>
	<ul id="albums" class="clearfix">
	  <?php foreach($created_month as $created => $month_year) { 
	  		$year = date('Y',$created);
	  		$month = date('m',$created);
	  	?>
	  <li class="<?php print ($created==0) ? 'first ' : ''; ?>album album-month album-<?php print $month_year; ?>">
	    <a href="album/nojs/<?php print $year; ?>/<?php print $month; ?>" class="use-ajax">
	      <img src="<?php print base_path().drupal_get_path('module', 'album_hierarchy'); ?>/images/folder.png"/>
	      <?php print $month_year; ?>
	    </a>
	  </li>
	  <?php } ?>
	</ul>
</div>